#!/usr/bin/env bash

doBuild(){
    source ../base_build_android.sh
    configFlags
    configTools
    #编译的库名称
    LIBNAME="libcurl"
    #输出路径
    PREFIX_OUT_PATH=".."

    if [ "$ARCH" = "arm64-v8a" ]; then
        ARCH_HOST=aarch64-linux-android
        ARCH_BUILD=aarch64-unknown-linux-gnu
    elif [ "$ARCH" = "armeabi-v7a" ]; then
        ARCH_BUILD=arm-unknown-linux-gnu
        ARCH_HOST=arm-linux-androideabi
    else
        echo "unsupport ARCH:$ARCH."
    fi

    ./configure  \
        --build=${ARCH_BUILD}  \
        --host=${ARCH_HOST}  \
        --with-sysroot=${PLATFORM}  \
        --enable-http  \
        --enable-proxy  \
        --enable-cookies  \
        --disable-ares  \
        --disable-debug  \
        --enable-optimize  \
        --enable-shared=yes  \
        --enable-static=no  \
        --disable-ftp  \
        --disable-file  \
        --disable-ldap  \
        --disable-ldaps  \
        --disable-rtsp  \
        --disable-dict  \
        --disable-telnet  \
        --disable-tftp  \
        --disable-pop3  \
        --disable-imap  \
        --disable-smtp  \
        --disable-gopher  \
        --disable-manual  \
        --disable-libcurl-option  \
        --disable-verbose  \
        --disable-ipv6  \
        --disable-soname-bump  \
        --enable-threaded-resolver  \
        --with-zlib  \
        --without-libssh2  \
        --without-winidn  \
        --without-winssl  \
        --without-darwinssl  \
        --without-ssl  \
        --without-polarssl  \
        --without-cyassl  \
        --without-axtls \
        --prefix=$(pwd)/build_out/Android/$ARCH

    #编译 并将编译结果拷贝到include和libs到对应的总目录
    makeAndCopy
}


archs=(
    'armeabi-v7a'
    'arm64-v8a'
)

num=${#archs[@]}
for ((i=0;i<$num;i++))
do
    doBuild ${archs[i]}
done